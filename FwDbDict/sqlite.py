﻿import os, requests, sqlite3, json
import hashlib
from collections import namedtuple
from FwFile import download_file

c_ALLOWED_TYPES = {
    'null', 'integer', 'real', 'text', 'blob'
}


def get_columns(db, s_table):
    a_cols = list(db.execute("PRAGMA table_info({})".format(s_table)))
    return [x[1] for x in a_cols]


def get_sqlite_dict(
    s_path, s_table, s_keys='', a_values=[],
    b_lowercase_keys=False, b_tuple_keys=False,
):
    ''' Read sqlite table into dictionary. First field will become keys. '''
    if s_path.lower().startswith('http'):
        s_path = download_file(s_path)
    else:
        s_path = s_path
    
    if not os.path.isfile(s_path):
        print("File not found: {}".format(s_path))
        return {}
        
    db = sqlite3.connect(s_path, uri=True)
    table_matches = list(db.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='{}';".format(s_table)))
    if not table_matches:
        print("Table not found: {} ({})".format(s_table, table_matches))
        db.close()
        return {}
    
    a_columns = get_columns(db, s_table)
    
    if not s_keys:
        print('columns: {}'.format(a_columns))
        a_possible_keys = [idx for idx in ['key', 'keys', 'id', 'index', 'idx'] if idx in a_columns]
        if a_possible_keys:
            s_keys = a_possible_keys[0]
            print('using "{}" as key'.format(s_keys))
        else:
            a_errors.append("Must specify a field to become the dictionary key: '{}'".format(s_keys))
    elif s_keys not in a_columns:
        print("Dictionary key field missing from table: '{}', {}".format(s_keys, a_columns))
        return {}
        
    if not a_values:
        a_values = [col for col in a_columns if col.lower() != s_keys.lower()]
    else:
        a_missing_values = [col for col in a_values if col not in a_columns]
        if a_missing_values:
            print("Value field(s) missing from table: '{}', {}".format(a_values, a_columns))
            return {}
        
    res = db.execute("select {}, {} from {}".format(s_keys, ', '.join(a_values), s_table))
    if len(a_values) == 1:
        if b_lowercase_keys and b_tuple_keys:
            e_table = {tuple(row[0].lower().split()): row[1] for row in res}
        elif b_lowercase_keys:
            e_table = {row[0].lower(): row[1] for row in res}
        elif b_tuple_keys:
            e_table = {tuple(row[0].split()): row[1] for row in res}
        else:
            e_table = {row[0]: row[1] for row in res}
    
    else:
        tn_value = namedtuple(s_keys, a_values)
        if b_lowercase_keys and b_tuple_keys:
            e_table = {tuple(row[0].lower().split()): tn_value._make(row[1:])
                       for row in res}
        elif b_lowercase_keys:
            e_table = {row[0].lower(): tn_value._make(row[1:])
                       for row in res}
        elif b_tuple_keys:
            e_table = {tuple(row[0].split()): tn_value._make(row[1:])
                       for row in res}
        else:
            e_table = {row[0]: tn_value._make(row[1:])
                       for row in res}

    try:
        del e_table[s_keys]
    except: pass

    db.close()
    return e_table


class SqliteDict():
    ''' Access sqlite table with a dictionary like interface for read only '''
    def __init__(self, s_path, s_table, s_keys='', a_values=[], s_name='', b_use_caching=True):
        ''' Description: create connection to sqlite db and present dict like interface for access
            @arg s_path: is the location of the sqlite file
            @arg s_table: is the name of the table in the sqlite file
            @arg s_keys: is the name of the field to use as the hash keys
            @arg a_values: is a list of the field(s) to use as the values
                           multiple fields will be returned as named tuples
        '''
        if s_path.lower().startswith('http'):
            self.url = s_path
            self.path = download_file(s_path)
        else:
            self.url, self.path = '', s_path
        
        self.table, self.key, self.fields = s_table, s_keys, a_values
        
        self.name = s_name if s_name else s_table
        self.db, self.open = None, False
        self.use_caching, self.cache = b_use_caching, {}
        self.columns, self.length = None, -1
        self.errors = self._check_parameters()
        
        if len(self.fields) > 1:
            self.tn = namedtuple(self.name, self.fields)
        else:
            self.tn = None
        
        if self.errors:
            self.db = None
            self.open = False
            #for s_error in self.errors:
            #    print(s_error)
    
    def __enter__(self):
        self.load()
        
    def __exit__(self, exc_type, exc_value, traceback):
        self.close()
        
    def load(self):
        if not self.errors:
            self.db = sqlite3.connect(self.path,  uri=True)
            cursor = self.db.execute('select * from {};'.format(self.table))
            self.length = len(cursor.fetchall())
            cursor.close()
        else:
            self.db = None
            for s_error in self.errors:
                print(s_error)
        self.open = True
    
    def close(self):
        try:
            self.db.close()
        except AttributeError:
            pass
        self.db = None
        self.open = False
        
    def __contains__(self, key):
        if self.use_caching and key in self.cache: return True
        if self.errors: return False
        
        s_query = "select {} from {} where {} = ? ;".format(', '.join(self.fields), self.table, self.key)
        result = list(self.db.execute(s_query, (key, )))
        return True if result else False
        
    def __len__(self):
        if self.length > 0: return self.length
        if self.errors: return 0
        cursor = self.db.execute('select * from {};'.format(self.table))
        self.length = len(cursor.fetchall())
        cursor.close()
        return self.length
        
    def get(self, key, default=None):
        if self.use_caching and key in self.cache: return self.cache[key]
        if self.errors: return default
    
        s_query = "select {} from {} where {} = ? ;".format(', '.join(self.fields), self.table, self.key)
        result = list(self.db.execute(s_query, (key, )))
        if not result:
            # I'm not sure if we should cache empty values or not
            if self.use_caching: self.cache[key] = default
            return default
        if self.tn:
            tn_result = self.tn._make(result[0])
            if self.use_caching: self.cache[key] = tn_result
            return tn_result
        else:
            if self.use_caching: self.cache[key] = result[0]
            return result[0]
        
    def keys(self):
        if self.errors: return None
        s_query = "select {} from {};".format(self.key, self.table)
        return [value[0] for value in (self.db.execute(s_query))]
        
    def values(self):
        if self.errors: return None
        s_query = "select {} from {};".format(', '.join(self.fields), self.table)
        result = self.db.execute(s_query)
        if not result: return default
        if self.tn:
            return [self.tn._make(value) for value in result]
        else:
            return list(result)
        
    def items(self):
        if self.errors: return None
        s_query = "select {}, {} from {};".format(self.key, ', '.join(self.fields), self.table)
        result = self.db.execute(s_query)
        if not result: return default
        if self.tn:
            return ((value[0], self.tn._make(value[1:])) for value in result)
        else:
            return ((value[0], value[1]) for value in result)
    
    def _check_parameters(self):
        a_errors = []
        if self.url and not self.path:
            a_errors.append("File not downloaded: '{}'".format(self.url))
            return a_errors
        elif not os.path.isfile(self.path):
            a_errors.append("File not found: '{}'".format(self.path))
            return a_errors
        
        self.db = sqlite3.connect(self.path, uri=True)
        table_matches = list(self.db.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='{}';".format(self.table)))
        if not table_matches:
            a_errors.append("Table not found: '{}' ({})".format(self.table, table_matches))
            self.close()
            return a_errors
    
        #self.db.row_factory = sqlite3.Row
        #cursor = self.db.execute("select * from {};".format(self.table))
        #row = cursor.fetchone()
        #self.columns = row.keys()
        self.columns = get_columns(self.db, self.table)
        
        #if not self.key:
        #    a_errors.append("Must specify a field to act as the dictionary key: '{}'".format(self.key))
        
        #if not self.fields:
            #a_errors.append("Must specify at least one field to act as the dictionary value: '{}'".format(self.fields))
        if not self.key:
            print('columns: {}'.format(self.columns))
            a_possible_keys = [idx for idx in ['key', 'keys', 'id', 'index', 'idx'] if idx in self.columns]
            if a_possible_keys:
                self.key = a_possible_keys[0]
                print('using "{}" as key'.format(self.key))
            else:
                a_errors.append("Must specify a field to act as the dictionary key: '{}'".format(self.key))
            
        elif self.key not in self.columns:
            a_errors.append("Dictionary key field missing from table: '{}', {}".format(self.key, self.columns))
            
        if not self.fields:
            self.fields = [col for col in self.columns if col.lower() != self.key.lower()]
        else:
            a_missing_values = [col for col in self.fields if col not in self.columns]
            if a_missing_values:
                a_errors.append("Value field(s) missing from table: '{}', {}".format(self.fields, self.columns))
            
        return a_errors


class SqliteFile():
    ''' Connect to sqlite table for both reading and writing. '''
    def __init__(self, s_path, s_table, s_name='',
                 s_key='',
                 a_fields=None, a_field_types=None,
                 a_field_req=None, a_field_default=None,
                 b_use_caching=False,
                 ):
        ''' Description: create connection to sqlite db and present dict like interface for access
            @arg s_path: is the location of the sqlite file
            @arg s_table: is the name of the table in the sqlite file
            @arg s_keys: is the name of the field to use as the hash keys
            @arg a_values: is a list of the field(s) to use as the values
                           multiple fields will be returned as named tuples
        '''
        self.path, self.table = s_path, s_table
        self.key = s_key
        self.fields = a_fields
        self.field_types = a_field_types if a_field_types else []
        self.field_req = a_field_req if a_field_req else []
        self.field_default = a_field_default if a_field_default else []
        
        self.name = s_name if s_name else s_table
        self.db, self.open = None, False
        self.columns, self.length = None, -1
        self.errors, self.tn = [], None
        self.sql_add, self.sql_del, self.sql_get = '', '', ''
        self.use_caching, self.cache = b_use_caching, {}
        
        self.errors = self._check_parameters()
        
        if len(self.fields) > 1:
            a_tn_props = self.fields
            if self.name not in a_tn_props:
                a_tn_props.insert(0, self.key)
            self.tn = namedtuple(self.name, a_tn_props)
        else:
            self.tn = None
        
        if self.errors:
            self.db, self.open = None, False
            for s_error in self.errors:
                print(s_error)
        
    def __enter__(self):
        print('opening connection to sqlite file')
        self.load()
        
    def __exit__(self, exc_type, exc_value, traceback):
        self.close()
        
    def load(self):
        """ create a database connection to a SQLite database """
        if not self.errors:
            try:
                self.db = sqlite3.connect(self.path, uri=True)
            except Error as e:
                print(e)
                print('Closing connection')
                self.db.close()
            finally:
                pass
        else:
            self.db = None
            for s_error in self.errors:
                print(s_error)
        self.open = True
    
    def close(self):
        try:
            self.db.close()
        except AttributeError:
            pass
        self.db = None
        self.open = False
        
    def _create_table(self):
        """ create a table from the create_table_sql statement
        """
        
        a_fields = ['id', self.key] + self.fields
        a_field_types = ['integer', 'text'] + self.field_types
        a_field_req = ['PRIMARY KEY AUTOINCREMENT NOT NULL', 'NOT NULL UNIQUE'] + self.field_req
        a_field_default = ['', ''] + self.field_default
        
        if len(a_field_types) < len(a_fields):
            a_field_types += ['text'] * (len(a_fields) - len(a_field_types))
        
        if len(a_field_req) < len(a_fields):
            a_field_req += [''] * (len(a_fields) - len(a_field_req))
            
        if len(a_field_default) < len(a_fields):
            a_field_default += [''] * (len(a_fields) - len(a_field_default))
            
        a_columns = []
        for s_fld, s_typ, s_req, s_def in zip(a_fields, a_field_types, a_field_req, a_field_default):
            s_field = s_fld
            if s_typ.lower() in c_ALLOWED_TYPES:
                s_field += ' {}'.format(s_typ)
            else:
                s_field += ' TEXT'
            
            if s_req == True:
                s_field += ' NOT NULL'
            elif s_req:
                s_field += ' {}'.format(s_req)
            
            if s_def:
                s_field += ' {}'.format(s_def)
            
            a_columns.append(s_field)
        
        sql = "CREATE TABLE IF NOT EXISTS {} (\n".format(self.table)
        sql += ", \n".join(a_columns)
        sql += "\n) ;"
        #print(sql)
        
        try:
            cursor = self.db.cursor()
            cursor.execute(sql)
            cursor.close()
        except Exception as e:
            print(e)
            self.errors.append(e)
            
        self.columns = a_fields
    
    def add_row(self, **key_and_values):
        if self.key not in key_and_values:
            print("No key specified")
            return None
        
        elements = tuple([key_and_values.get(col) for col in self.columns if col != 'id'])
        cursor = self.db.cursor()
        try:
            cursor.execute(self.sql_add, elements)
            self.db.commit()
            #print('row added', cursor.lastrowid, elements)
        except Exception as e:
            print(e)
        
        if self.use_caching:
            key = key_and_values.get(self.key)
            result = [key_and_values.get(col) for col in self.fields]
            if self.tn:
                tn_result = self.tn._make(result)
                self.cache[key] = tn_result
            else:
                self.cache[key] = result[0]
        
        last_row = cursor.lastrowid
        cursor.close()
        return last_row
    
    def del_row_by_key(self, key):
        if self.use_caching and key in self.cache:
            del self.cache[key]
        try:
            cursor = self.db.cursor()
            cursor.execute(self.sql_del, (key,))
            cursor.close()
            self.db.commit()
        except Exception as e:
            print(e)
        
    def del_all(self):
        try:
            self.cache = {}
            cursor = self.db.cursor()
            cursor.execute('DELETE FROM {};'.format(self.table))
            cursor.close()
            self.db.commit()
        except Exception as e:
            print(e)

    def __contains__(self, key):
        if self.use_caching and key in self.cache: return True
        if self.errors: return False
        
        #s_query = "select {} from {} where {} = ? ;".format(', '.join(self.fields), self.table, self.key)
        result = list(self.db.execute(self.sql_get, (key, )))
        return True if result else False
                           
    def __delitem__(self, key):
        if self.errors: return False
        self.del_row_by_key(key)
        
    def __setitem__(self, key, value):
        if self.errors: return False
        if not key: return False
        
        if isinstance(value, dict):
            kwargs = {self.key:key}
            kwargs.update(value)
        elif isinstance(value, (list, tuple)):
            if len(value) > len(self.fields):
                kwargs = {col: val for col, val in zip(self.columns, value)}
                kwargs[self.key] = key
            else:
                kwargs = {col: val for col, val in zip(self.fields, value)}
                kwargs[self.key] = key
        else:
            kwargs = {self.key:key, self.fields[0]: value}
        self.add_row(**kwargs)
        
    def __len__(self):
        if self.length > 0: return self.length
        if self.errors: return 0
        cursor = self.db.execute('select * from {};'.format(self.table))
        self.length = len(cursor.fetchall())
        cursor.close()
        return self.length
        
    def get(self, key, default=None):
        if self.use_caching and key in self.cache: return self.cache[key]
        if self.errors: return default
    
        #s_query = "select {} from {} where {} = ? ;".format(', '.join(self.fields), self.table, self.key)
        result = list(self.db.execute(self.sql_get, (key, )))
        #print(result)
        if not result:
            # I'm not sure if we should cache empty values or not
            if self.use_caching: self.cache[key] = default
            return default
        if self.tn:
            tn_result = self.tn._make(result[0])
            if self.use_caching: self.cache[key] = tn_result
            return tn_result
        else:
            if self.use_caching: self.cache[key] = result[0]
            return result[0]
        
    def keys(self):
        if self.errors: return None
        s_query = "SELECT {} FROM {};".format(self.key, self.table)
        return [value[0] for value in (self.db.execute(s_query))]
        
    def values(self):
        if self.errors: return None
        s_query = "SELECT {} FROM {};".format(', '.join(self.fields), self.table)
        result = self.db.execute(s_query)
        if not result: return default
        if self.tn:
            return [self.tn._make(value) for value in result]
        else:
            return list(result)
        
    def items(self):
        if self.errors: return None
        s_query = "SELECT {}, {} FROM {};".format(self.key, ', '.join(self.fields), self.table)
        result = self.db.execute(s_query)
        if not result: return default
        if self.tn:
            return ((value[0], self.tn._make(value[1:])) for value in result)
        else:
            return ((value[0], value[1]) for value in result)
    
    def _check_parameters(self):
        a_errors = []
        if not os.path.isdir(os.path.dirname(self.path)):
            a_errors.append("Folder not found: '{}'".format(os.path.dirname(self.path)))
            return a_errors
        
        #if not os.path.isfile(self.path):
        ''' If sqlite file doesn't exist yet, try to create it ''' 
        try:
            self.db = sqlite3.connect(self.path, uri=True)
        except Exception as e:
            a_errors.append(e)
            return a_errors
            
        table_matches = list(self.db.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='{}';".format(self.table)))
        if not table_matches:
            ''' Table doesn't exist yet. Create it ''' 
            if not self.key:
                a_errors.append("Must specify a field to act as the dictionary key: '{}'".format(self.key))

            if not self.fields:
                a_errors.append("Must specify at least one field to act as the dictionary value: '{}'".format(self.fields))
            if a_errors:
                self.close()
                return a_errors
            
            self._create_table()
            if self.errors:
                self.close()
                return self.errors
    
        else:
            #self.db.row_factory = sqlite3.Row
            #cursor = self.db.execute("SELECT * FROM {};".format(self.table))
            #row = cursor.fetchone()
            #self.columns = row.keys()
            self.columns = get_columns(self.db, self.table)
            
            if not self.key:
                print('columns: {}'.format(self.columns))
                a_possible_keys = [idx for idx in ['key', 'keys', 'id', 'index', 'idx'] if idx in self.columns]
                if a_possible_keys:
                    self.key = a_possible_keys[0]
                    print('using "{}" as key'.format(self.key))
                else:
                    a_errors.append("Must specify a field to act as the dictionary key: '{}'".format(self.key))
            
            elif self.key not in self.columns:
                a_errors.append("Dictionary key field missing from table: '{}', {}".format(self.key, self.columns))
            
            if not self.fields:
                self.fields = [col for col in self.columns if col.lower() != self.key.lower()]
            else:
                a_missing_values = [col for col in self.fields if col not in self.columns]
                if a_missing_values:
                    a_errors.append("Value field(s) missing from table: '{}', {}".format(self.fields, self.columns))
        
        s_columns = ','.join(self.columns[1:])
        s_values = ','.join(['?'] * (len(self.columns) - 1))
        self.sql_add = "REPLACE INTO {}({}) VALUES({}) ;".format(self.table, s_columns, s_values)
        self.sql_del = "DELETE FROM {} WHERE id=? ;".format(self.table)
        self.sql_get = "SELECT {} FROM {} WHERE {} = ? ;".format(', '.join(self.fields), self.table, self.key)
        
        return a_errors
