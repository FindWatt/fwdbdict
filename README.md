### What is this repository for? ###

* This repository is for extracting sql tables as dictionaries in python or reading their records in a dictionary-like manner.
* THIS IS FOR READ-ONLY ACCESS. Writing to or updating the sqlite file is not supported.
* 0.0.3

### How do I get set up? ###

* pip install git+https://bitbucket.org/FindWatt/fwdbdict

### How do I use it ###

Example usage to extract table to dictionary in memory:

```
#!python

from FwDbDict import sqlite

e_dict = sqlite.get_sqlite_dict(url_or_file, tablename, key_field, value_fields_list)

assert mykey in e_dict

a_list = sorted(e_dict.items(), operator.itemgetter(0))
```

Example usage to access table like dictionary from file:

```
#!python
db_dict_like = SqliteDict(url_or_file, tablename, key_field, value_fields_list)

with db_dict_like:
	assert mykey in db_dict_like
	value = db_dict_like.get(mykey, '')
	e_filtered_dict = {key: value for key, value in db_dict_like.items() if value == criteria}
	i_rows = len(db_dict_like)
	a_keys = db_dict_like.keys()
	a_values = db_dict_like.values()
```

or

```
#!python

db_dict_like = SqliteDict(url_or_file, tablename, key_field, value_fields_list)

db_dict_like.load()
assert mykey in db_dict_like
value = db_dict_like.get(mykey)
e_filtered_dict = {key: value for key, value in db_dict_like.items() if value == criteria}
i_rows = len(db_dict_like)
a_keys = db_dict_like.keys()
a_values = db_dict_like.values()
db_dict_like.close()

```

If multiple value fields are specified, the dictionary values will be namedtuples. If a single value field is specified, the dictionary values will be the plain values from that field.

By default, values that are accessed by .get() are cached in a regular dictionary for fast re-access. Keys that are checked against cache by .__contains__(), but not added to it. Caching can be turned off with the b_use_caching parameter during init, or the .use_caching property. Currently the cache is not automatically cleared or pruned or emptied after the connection is closed.