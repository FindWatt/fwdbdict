import os, re, sys
import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'FwDbDict/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = re.findall("\d+\.\d+\.\d+", version_line)[0]


class CustomInstall(install):
    """
    @description: Installs external FindWatt dependencies before installing
    the current package.
    """
    def run(self):
        self.install_fw_dependencies()
        install.run(self)

    def install_fw_dependencies(self):
        pass
        if sys.version_info.major == 2:
            os.system("pip install git+https://bitbucket.org/FindWatt/fwfile")
            #os.system("pip install git+https://bitbucket.org/FindWatt/fwutil")
        elif sys.version_info.major == 3:
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fwfile")
            #os.system("pip3 install git+https://bitbucket.org/FindWatt/fwutil")


setuptools.setup(
    name="FwDbDict",
    version=__version__,
    url="https://bitbucket.org/FwPyClassification/fwdbdict",

    author="FindWatt",

    description="Identify catalog feeds of known types by looking at field names",
    long_description=open('README.md').read(),

    packages=setuptools.find_packages(),
    include_package_data=True,
    package_data={'': []},
    py_modules=['FwDbDict'],
    zip_safe=False,
    platforms='any',

    install_requires=[],
    cmdclass={'install': CustomInstall}
)
